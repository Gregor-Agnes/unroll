# -*- mode: ruby -*-
# vi: set ft=ruby :

# Requires to perform this first once:
# `vagrant plugin install vagrant-winnfsd`
# `vagrant plugin install vagrant-bindfs`

Vagrant.configure("2") do |config|
    config.vm.box = "ArminVieweg/ubuntu-xenial64-lamp"

    config.vm.network "forwarded_port", guest: 80, host: 8080

    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder ".", "/var/nfs", type: "nfs"

    config.bindfs.bind_folder "/var/nfs", "/vagrant"
    config.bindfs.bind_folder "/var/nfs", "/var/www/html/typo3conf/ext/unroll"

    config.bindfs.default_options = {
        force_user:   "vagrant",
        force_group:  "www-data",
        perms:        "u=rwX:g=rwX:o=rD"
    }

    config.vm.network "private_network", type: "dhcp"

    config.vm.provider "virtualbox" do |vb|
        vb.memory = 4096
        vb.cpus = 2
    end

    # Run always
    config.vm.provision "shell", run: "always", inline: <<-SHELL
        cd ~
        sudo composer self-update --no-progress
    SHELL

    # Run once (installs TYPO3 CMS in /var/www/html)
    config.vm.provision "shell", inline: <<-SHELL
        cd /var/www/html
        echo "{}" > composer.json

        # Add packages to required section in composer.json
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "typo3/cms" "^8.7"
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "helhum/typo3-console" "^4.5"

        echo "Fetching TYPO3 CMS 8.7 using composer and install it with typo3_console..."
        composer update --no-progress -n -q
        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3_unroll" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "EXT:unroll Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush

        # Add PSR-4 autoloading entries in composer.json
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["autoload"]["psr-4"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "InstituteWeb\\\\Unroll\\\\" typo3conf/ext/unroll/Classes
        composer dump -o

        chmod 2775 . ./typo3conf ./typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        php typo3/cli_dispatch.phpsh extbase extension:install unroll
    SHELL

end
